﻿using System;

namespace Global
{
	[System.Serializable]
	public class User
	{
		private String userName{get;set;}

		public User (String userName)
		{
			this.userName = userName;
		}
			
	}
}

