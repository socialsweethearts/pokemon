﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Global
{
	public class Navigation: MonoBehaviour{

		public static void openArena(){
			SceneManager.LoadScene(Global.AppConstants.scene_arena, mode: LoadSceneMode.Single);
		}
	}
}