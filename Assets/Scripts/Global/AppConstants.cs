﻿using UnityEngine;
using System.Collections;


namespace Global
{
	public class AppConstants{

		/** Scenes **/

		public static string scene_menu = "Menu";
		public static string scene_arena = "Arena";

		/** User **/


		public static User user;

		public static void CreateEmptyUser(){
			user = new User("Arseny");
		}
	}
}
